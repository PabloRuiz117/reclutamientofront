import { Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule, MatSnackBarRef } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.css'],
  standalone: true,
  imports: [MatButtonModule, MatSnackBarModule],
})
export class SnackbarComponent {
  snackBarRef = inject(MatSnackBarRef);

  constructor(private _router: Router) {}

  onViewFavorites(): void {
    this._router.navigate(['pokedex/favorites']);
  }
}
