import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokedexRoutingModule } from './pokedex-routing.module';
import { HomePokedexComponent } from './pages/home-pokedex.component';
import { MaterialModule } from '../material/material.module';
import { SearchComponent } from './components/search/search.component';
import { FavoritesComponent } from './components/favorites/favorites.component';
import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';

@NgModule({
  declarations: [
    HomePokedexComponent,
    SearchComponent,
    FavoritesComponent,
    ListComponent,
    DetailComponent,
  ],
  imports: [CommonModule, PokedexRoutingModule, MaterialModule, CarouselModule],
})
export class PokedexModule {}
