import { Component } from '@angular/core';

@Component({
  selector: 'app-home-pokedex',
  templateUrl: './home-pokedex.component.html',
  styleUrls: ['./home-pokedex.component.css'],
})
export class HomePokedexComponent {
  public menuItems = [
    { label: 'Listado', icon: 'home', url: './list' },
    // { label: 'Buscar', icon: 'search', url: './search' },
    { label: 'Favoritos', icon: 'favorite', url: './favorites' },
  ];
}
