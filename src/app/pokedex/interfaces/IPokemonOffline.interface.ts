export interface IPokemonOffline {
  id: number;
  name: string;
  experience: number;
  height: number;
  weight: number;
}
