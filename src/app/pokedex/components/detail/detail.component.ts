import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Pokemon } from '../../interfaces/IAPIResult.interface';
import { PokemonDetail } from '../../interfaces/IPokemonDetail.interface';
import { CarouselConfig } from 'ngx-bootstrap/carousel';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
  providers: [
    {
      provide: CarouselConfig,
      useValue: { interval: 3000, noPause: true, showIndicators: true },
    },
  ],
})
export class DetailComponent {
  pokemon!: PokemonDetail;
  picture: string = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: PokemonDetail) {
    setTimeout(() => {
      this.pokemon = data;
      this.picture = this.pokemon.sprites.back_default;
    }, 1000);
  }
}
