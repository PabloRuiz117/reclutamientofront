import { Component, OnInit } from '@angular/core';
import { PokedexService } from '../../services/pokedex.service';
import { APIResult, Pokemon } from '../../interfaces/IAPIResult.interface';
import { MatDialog } from '@angular/material/dialog';
import { DetailComponent } from '../detail/detail.component';
import { PokemonDetail } from '../../interfaces/IPokemonDetail.interface';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent } from '../../../snackbar/snackbar.component';
import { IPokemonOffline } from '../../interfaces/IPokemonOffline.interface';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  private _pokemonListOffline: IPokemonOffline[] =
    JSON.parse(localStorage.getItem('favorites')!) || [];
  pokemonList: Pokemon[] = [];
  pokemon!: PokemonDetail;
  page = 1;
  pageSize = 100;
  totalPokemon = 0;

  constructor(
    private _pokemonService: PokedexService,
    private _dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.getPokemonList();
  }

  getPokemonList(): void {
    this._pokemonService
      .getPokemonList(this.page, this.pageSize)
      .subscribe((data) => {
        this.pokemonList = data.results;
        this.totalPokemon = data.count;
      });
  }

  onPageChange(event: any): void {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getPokemonList();
  }

  onViewDetail(url: string): void {
    const segment = url.split('/');

    const pokemonId = segment[6];

    this._pokemonService.getPokemonById(pokemonId).subscribe((pokemon) => {
      this.pokemon = pokemon;

      this._dialog.open(DetailComponent, {
        width: '600px',
        enterAnimationDuration: 300,
        exitAnimationDuration: 500,
        data: pokemon,
      });
    });
  }

  onAddFavorites(pokemon: Pokemon): void {
    const segment = pokemon.url.split('/');

    const pokemonId = segment[6];

    this._pokemonService.getPokemonById(pokemonId).subscribe((pokemonRes) => {
      const pokemonOffline: IPokemonOffline = {
        id: pokemonRes.id,
        name: pokemonRes.name,
        experience: pokemonRes.base_experience,
        height: pokemonRes.height,
        weight: pokemonRes.weight,
      };

      this._pokemonListOffline.push(pokemonOffline);

      const listPokemonsJson = JSON.stringify(this._pokemonListOffline);

      localStorage.setItem('favorites', listPokemonsJson);
    });

    this._snackBar.openFromComponent(SnackbarComponent, {
      duration: 3 * 1000,
    });
  }
}
