import { Component } from '@angular/core';
import { IPokemonOffline } from '../../interfaces/IPokemonOffline.interface';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css'],
})
export class FavoritesComponent {
  listFavorites: IPokemonOffline[] =
    JSON.parse(localStorage.getItem('favorites')!) || [];
}
