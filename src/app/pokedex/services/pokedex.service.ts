import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/enviroments';
import { APIResult, Pokemon } from '../interfaces/IAPIResult.interface';
import { PokemonDetail } from '../interfaces/IPokemonDetail.interface';

@Injectable({
  providedIn: 'root',
})
export class PokedexService {
  private _baseUrl: string = environment.baseUrl;

  constructor(private _httpClient: HttpClient) {}

  getPokemonList(page: number, pageSize: number): Observable<APIResult> {
    const offset = (page - 1) * pageSize;
    const url = `${this._baseUrl}?offset=${offset}&limit=${pageSize}`;
    return this._httpClient.get<APIResult>(url);
  }

  getPokemonById(pokemonId: string): Observable<PokemonDetail> {
    return this._httpClient.get<PokemonDetail>(`${this._baseUrl}/${pokemonId}`);
  }
}
