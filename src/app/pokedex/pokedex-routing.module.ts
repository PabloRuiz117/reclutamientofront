import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePokedexComponent } from './pages/home-pokedex.component';
import { SearchComponent } from './components/search/search.component';
import { FavoritesComponent } from './components/favorites/favorites.component';
import { ListComponent } from './components/list/list.component';

const routes: Routes = [
  {
    path: '',
    component: HomePokedexComponent,
    children: [
      { path: 'list', component: ListComponent },
      { path: 'search', component: SearchComponent },
      { path: 'favorites', component: FavoritesComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PokedexRoutingModule {}
